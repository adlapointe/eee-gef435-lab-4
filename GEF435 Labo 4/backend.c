/*
 * backend.c
 *
 * Implements the basic functions for a bank system.
 * Implemente les fonctions de base d'un systeme bancaire.
 *
 *  Created on: Oct 27, 2014
 *      Author: Patrick Tohill
 */

#include "backend.h"

/*
 * Initialize the accounts.
 * Initialize les comptes.
 *
 */
void initialize(){
	system("./lab4.sh");
}

/*
 * Simulates delays at the bank level.
 * Simule un d�lais au niveau de la banque.
 */
void bankDelay(){
	int i;
	for(i=0; i<SPINS; i++);
}

/*
 *
 * Returns the account balance for the account number in parameter.
 * Retourne la balance du compte correspondant au num�ro de compte en parametre.
 *
 */
int getCurrentBalance(int accountNo){
	FILE * account;
	char balance[100];
	account = openAccount(accountNo, "r");
	if(account == NULL){
		printf("ERROR GETTING BANK BALANCE");
		return -1;
	}else{
		fgets(balance,100,account);
	}
	return atoi(balance);
}

/*
 * Opens an account provided:
 * accountNo: the account number; and
 * fileMode: the file mode (r=read, w=write, x=execute).
 *
 * Returns a file handle.
 *
 * Ouvre le compte etant donne:
 * accountNo: le num�ro de compte; et
 * fileMode: le mode du fichier (r=lecture, w=ecriture, x=execution)
 *
 * Retourne une poignee de fichier.
 *
 */
FILE * openAccount(int accountNo, const char* fileMode){
	FILE *account;
	char accountName[100];
	char stringNum[100];
	sprintf(stringNum, "%d", accountNo);
	strcpy(accountName, ACCOUNTS_PATH);
	strcat(accountName, "account");
	strcat(accountName, stringNum);
	account = fopen(accountName, fileMode);
	if(account == NULL){
		printf("Failed to open the account No. %d\n", accountNo);
		return NULL;
	}else{
		return account;
	}
}

/*
 * Adds the amount to the account specified by the accountNo.
 *
 * Ajoute le montant amount au compte specifie par accountNo.
 *
 */
void deposit(int accountNo, int amount){
	FILE * destination;
	bankDelay();
	int initial = getCurrentBalance(accountNo);
	destination = openAccount(accountNo, "w");
	printf("Depositing %d into accountNo[%d]:  ", amount, accountNo);
	fprintf(destination, "%d", amount+initial);
	fclose(destination);

}

/*
 * Decreases by the amount the balance of the account specified in accountNo.
 *
 * D�bite d'amount le compte specifie par accountNo.
 *
 */
void withdraw(int accountNo, int amount){
	FILE * source;
	bankDelay();
	int initial = getCurrentBalance(accountNo);
	source = openAccount(accountNo, "w");
	printf("Withdrawing %d from account[%d]. \n", amount, accountNo);
	fprintf(source, "%d", initial-amount);
	fclose(source);
}

/*
 * Prints the accounts balance.
 *
 * Affiche la balance des comptes.
 *
 */

void printAccounts(){
	int i;
	printf("\n\n");
	printf("|---------------------------------------------------------|\n");
	printf("|                    Financial update                     |\n");
	printf("|---------------------------------------------------------|\n");
	printf("Bank balance is $%d.\n", getCurrentBalance(0));
	printf("|---------------------------------------------------------|\n");
	printf("|Employee balances:                                       |\n");
	for (i=1; i<=NUM_OF_ACCOUNTS; i++){
		printf("       account%d is now $%d                       \n", i,getCurrentBalance(i));
	}
	printf("|---------------------------------------------------------|\n\n\n");


}
