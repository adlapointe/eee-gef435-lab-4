#!/bin/sh

rm -rf ./accounts/
mkdir accounts

for ((i=0; i<=15; i++))
do
	accountName="account$i"
	echo "Creating $accountName account"
	echo "0" > accounts/$accountName
done
