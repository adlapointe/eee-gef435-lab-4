#include <string.h>

#define SPINS 10000000
#define NUM_OF_ACCOUNTS 15
#define ACCOUNTS_PATH "./accounts/"
int systemBalance;

void initialize();
void bankDelay();
int getCurrentBalance(int accountNo);
FILE * openAccount(int accountNo, const char* fileMode);
void withdraw(int accountNo, int amount);
void deposit(int accountNo, int amount);
void printAccounts();

